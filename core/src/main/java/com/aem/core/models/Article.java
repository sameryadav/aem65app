package com.aem.core.models;

import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.export.json.ComponentExporter;

@Model(adaptables = SlingHttpServletRequest.class , adapters = {ComponentExporter.class}, resourceType = Article.RESOURCE_TYPE)
@Exporter(name = "jackson", extensions = "json")
public class Article implements ComponentExporter {

	private static final Logger logger = LoggerFactory.getLogger(Article.class);
	
	//protected static final String RESOURCE_TYPE="aem65app/components/content/contentfragment";
	protected static final String RESOURCE_TYPE="/conf/aem65app/settings/wcm/templates/content-page";

	private static final String PN_TITLE = "title";
	private static final String PN_DESCRIPTION = "description";
	
	@SlingObject
	private Resource resource;
	
	@ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named(PN_TITLE)
	String title;
	
	@ValueMapValue(injectionStrategy = InjectionStrategy.OPTIONAL)
	@Named(PN_DESCRIPTION)
	String description;
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}	
	
	@Override
	public String getExportedType() {
		logger.info("Article 1: "+this.RESOURCE_TYPE);
		return this.RESOURCE_TYPE;
	}

}
