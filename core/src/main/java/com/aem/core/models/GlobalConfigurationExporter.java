package com.aem.core.models;

/*
 *  Copyright 2017 McAfee, LLC
 *
 *  
 */
// Java Imports
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
// Apache Imports
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Exporter;

// Log4j Imports
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p> An exporter to generate JSON of global variables for different regions using 
 * 'Global Configuration' components. </p>
 * 
 * <p>Sample URL: http://localhost:4502/content/enterprise/en-us/jcr:content.globalconfig.json</p>
 * 
 * 
 */
@Model(adaptables = Resource.class, resourceType = "aem65app/components/structure/page", defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", selector="globalconfig", extensions = "json")
public class GlobalConfigurationExporter {
	
	private static final Logger logger = LoggerFactory.getLogger(GlobalConfigurationExporter.class);
	
	@Self 
	private Resource resource;
	
	// list to hold all the global variables for a region
	private List<Map<String, String>> globalConfig;
	
	@PostConstruct
	protected void init() {
		// Populate global variables authored on the page in a JSON object
		if (null != resource) {
			populateGlobalVariables();
		}
	}
	
	/**
	 * This method is used to fetch the global variables authored on a page
	 * 
	 */
	private void populateGlobalVariables() {

		logger.info("Entering GlobalConfigurationExporter.populateGlobalVariables method.");
		globalConfig = new ArrayList<>();
		
		try {/*
			
			// fetch mainpar node for a page
			Resource mainparResource = resource.getChild(ApplicationConstants.MAIN_PARSYS);
			
			if (null != mainparResource) {
				Map<String, String> globalVariable = new HashMap<>();
				Iterator<Resource> componentsIterator = mainparResource.listChildren();
				
				// iterate through all the components and fetch global variables from 'Global Configuration' component
				while (componentsIterator.hasNext()) {
					Resource componentResource = componentsIterator.next();
					ValueMap valueMap = componentResource.adaptTo(ValueMap.class);
					if (null != valueMap) {
						String resourceType = valueMap.get(ApplicationConstants.PN_RESOURCE_TYPE, String.class);
						
						if (ApplicationConstants.CN_GLOBAL_CONFIG.equalsIgnoreCase(resourceType)) {
							Resource globalVarResource = componentResource.getChild(ApplicationConstants.NN_GLOBAL_VAR);
							if (null != globalVarResource) {
								Iterator<Resource> globalVarIterator = globalVarResource.listChildren();
								while (globalVarIterator.hasNext()) {
									ValueMap globalVarValueMap = (globalVarIterator.next()).adaptTo(ValueMap.class);
									if (null != globalVarValueMap) {
										String varName = globalVarValueMap.get(ApplicationConstants.PN_VAR_NAME, String.class);
										String varValue = globalVarValueMap.get(ApplicationConstants.PN_VAR_VALUE, String.class);
										if(varValue == null) {
											varValue = "";
										}
										if (StringUtils.isNotBlank(varName)) {
											globalVariable.put(varName,varValue);
										}
									}
								}
							}
							
						}
						
					}
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Final list of global variables :: {}.", globalVariable);
				}
				globalConfig.add(globalVariable);
			}
			
		*/} catch (Exception e) {
			logger.error("Execption occured while populating global variables :: {}.", e.getMessage(), e);
		}
		
		logger.info("Exiting GlobalConfigurationExporter.populateGlobalVariables method.");
	}
	
	/**
	 * <p>Getter for global variable list</p>
	 * @return globalConfig - list of global configurations (List<Map>)
	 */
	public List<Map<String, String>> getGlobalConfig() {
		return globalConfig;
	}
	
}
