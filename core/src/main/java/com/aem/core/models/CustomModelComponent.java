package com.aem.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.ExporterOption;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Required;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

@Model(adaptables = Resource.class, resourceType = { "aem65app/components/content/contentfragment" }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = "json")
public class CustomModelComponent {

	private static final Logger logger = LoggerFactory.getLogger(CustomModelComponent.class);
	//content/dam/aem65app/cf-1/jcr:content/data/master

	@Self 
	private Resource resource;
	
	private String message;
	@Inject
	private SlingSettingsService settings;

	@Inject @Named(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY) @Default(values="No resourceType")
	protected String resourceType;

	@Inject
	@ValueMapValue
	@Named("jcr:title")
	String title;

	@Inject
	@ValueMapValue
	@Named("jcr:description")
	String description;

	@Inject
	@Named("sling:resourceType")
	String slingResourceType;

	public String getSlingResourceType() {
		logger.info("CustomModelComponent 1: "+slingResourceType);
		return slingResourceType;
	}

	@JsonIgnore
	public String getTitle() {
		return title;
	}


	@PostConstruct
	protected void init() {
		message = "\tHello World!\n";
		message += "\tThis is instance: " + settings.getSlingId() + "\n";
		message += "\tResource type is: " + resourceType + "\n";

		description="This is Test Description";
		
		logger.info("resource 1: "+resource);

		if (null != resource) {
			getResourceListHelper(resource);
		}

	}

	public void getResourceListHelper(Resource parentResource) {
		logger.info("inside resource helper: ");
		Iterator resourceChildrenIter = parentResource.listChildren(); 
		Resource childResource;
		while(resourceChildrenIter.hasNext()) {
			
			childResource = (Resource) resourceChildrenIter.next();
			logger.info("childResource-1: "+childResource);
			ValueMap childProperties = (ValueMap) childResource.adaptTo(ValueMap.class);
			logger.info("childProperties-1: "+childProperties);
		}
	}

	/*public void getResourceListHelper(Resource parentResource, String targetSearchPropertyKey, String targetSearchPropertyValue, List components) {    

		Resource childResource;
		for (Iterator resourceChildrenIter = parentResource.listChildren(); resourceChildrenIter.hasNext(); 
				getResourceListHelper(childResource, targetSearchPropertyKey, targetSearchPropertyValue, components)) {

			childResource = (Resource) resourceChildrenIter.next();
			ValueMap childProperties = (ValueMap) childResource.adaptTo(ValueMap.class);

			if (targetSearchPropertyValue.equals(childProperties.get(targetSearchPropertyKey, String.class))) {
				components.add(childResource);
				logger.info("components-1: "+components);
			}
		}

	}*/

	public String getMessage() {
		return message;
	}

	public String getDescription() {
		return description;
	}
}