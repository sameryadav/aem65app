package com.aem.core.models;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentVariation;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.*;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
import javax.annotation.PostConstruct;
import javax.jcr.Node;
import javax.jcr.Session;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.jcr.Session; 
import javax.jcr.Property;

@Model(
		adaptables = { SlingHttpServletRequest.class },
		resourceType = "jurassicworld/components/content/sling-model",
		defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
		)
@Exporter(name = "jackson", extensions = "json", options = {
		@ExporterOption(name = "MapperFeature.SORT_PROPERTIES_ALPHABETICALLY", value = "true"),
		@ExporterOption(name = "SerializationFeature.WRITE_DATES_AS_TIMESTAMPS", value="false")
})

public class SlingExporterModel {
	private Session session;
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	@Self
	private SlingHttpServletRequest request;

	@Self @Via("resource")
	private Resource resource;


	// Inject OSGi services
	@OSGiService
	@Required
	private QueryBuilder queryBuilder;
	@SlingObject
	@Required
	private ResourceResolver resourceResolver;
	private Page page;
	private List<Map<String, String>> items = new ArrayList<Map<String,String>>();
	private com.adobe.cq.dam.cfm.ContentFragment fragment;

	@ValueMapValue
	@Named("jcr:title")
	@Required
	private String title;

	@ValueMapValue
	@Optional
	private String pageTitle;

	public String getTitle() {
		return StringUtils.defaultIfEmpty(pageTitle, title);
	}
	@PostConstruct

	private void init() {
		try {

			String scene7File = "";
			String scene7Domain = "";
			String scene7Type = "";
			String scene7Path = "";
			String scene7Name ="";
			String scene7Folder = "";
			String contentName ="";
			String uniqueDinoId ="";
			String collectionName="";
			String zoneDino="";
			int flag=0;
			page = resourceResolver.adaptTo(PageManager.class).getContainingPage(resource);
			Session session = resourceResolver.adaptTo(Session.class);

			final Map<String, String> map = new HashMap<String, String>();

			// Injected fields can be used to define logic
			if(page.isValid()) {
				map.put("path", page.getPath());

				map.put("property", "sling:resourceType");
				map.put("Property.value",  "VideoContentAPI/components/page/test-video-component-page4");
			}
			Query query = queryBuilder.createQuery(PredicateGroup.create(map), resourceResolver.adaptTo(Session.class));
			log.info("query");
			final SearchResult result = query.getResult();
			Iterator<Node> nodeItr = result.getNodes();
			ContentVariation variant=null;
			ContentElement element =null;
			while(nodeItr.hasNext()) {
				Node node=nodeItr.next();
				if(node.hasProperty("fragmentPath")) {
					String path = node.getProperty("fragmentPath").getValue().getString();
					if(!StringUtils.isEmpty(path))
					{
						Resource fragmentResource = resourceResolver.getResource(path);
						if(fragmentResource !=null) {
							fragment =fragmentResource.adaptTo(com.adobe.cq.dam.cfm.ContentFragment.class);
							Iterator<ContentElement> iterator =fragment.getElements();
							Map<String,HashMap<String,String>> fragmentMap = new HashMap<String,HashMap<String,String>>();
							Map<String,String> masterMap = new HashMap<String,String>();
							while(iterator.hasNext()) {
								element= iterator.next();
								String damPath= element.getContent();
								if(damPath.contains("/content/dam/"))
								{
									damPath = damPath.substring(1) + "/jcr:content/metadata";
									log.info("damPath" + damPath);
									Node root = session.getRootNode();
									Node dataPathNode = root.getNode(damPath);
									Boolean propcontentName = dataPathNode.hasProperty("dc:contentName");
									Boolean propuniqueDinoId = dataPathNode.hasProperty("dc:uniqueDinoId");
									Boolean propcollectionName = dataPathNode.hasProperty("dc:collectionName");
									Boolean propzoneDino = dataPathNode.hasProperty("dc:zoneDino");
									Boolean propScene7File = dataPathNode.hasProperty("dam:scene7File");
									Boolean propScene7Domain = dataPathNode.hasProperty("dam:scene7Domain");
									Boolean propScene7Type = dataPathNode.hasProperty("dam:scene7Type");
									Boolean propscene7Name = dataPathNode.hasProperty("dam:scene7Name");
									Boolean propscene7Folder = dataPathNode.hasProperty("dam:scene7Folder");
									if(propcontentName)
									{
										Property contentNameProperty = dataPathNode.getProperty("dc:contentName");
										contentName = contentNameProperty.getValue().toString();
									}
									if(propuniqueDinoId)
									{
										Property uniqueDinoIdProperty = dataPathNode.getProperty("dc:uniqueDinoId");
										uniqueDinoId = uniqueDinoIdProperty.getValue().toString();
									}
									if(propcollectionName)
									{
										Property collectionNameProperty = dataPathNode.getProperty("dc:collectionName");
										collectionName = collectionNameProperty.getValue().toString();
									}
									if(propzoneDino)
									{
										Property zoneDinoProperty = dataPathNode.getProperty("dc:zoneDino");
										zoneDino = zoneDinoProperty.getValue().toString();
									}
									if (propScene7File && propScene7Domain && propScene7Type && propscene7Name && propscene7Folder) {
										log.info("inside if");  
										scene7Name = (dataPathNode.hasProperty("cq:name"))? dataPathNode.getName().toString():"";
										Property scene7FileProperty = dataPathNode.getProperty("dam:scene7File");
										scene7File = scene7FileProperty.getValue().toString();
										Property scene7DomainProperty = dataPathNode.getProperty("dam:scene7Domain");
										scene7Domain = scene7DomainProperty.getValue().toString();
										Property scene7FolderProperty = dataPathNode.getProperty("dam:scene7Folder");
										scene7Folder = scene7FolderProperty.getValue().toString();

										if (propScene7Type) {
											Property scene7TypeProperty = dataPathNode.getProperty("dam:scene7Type");
											scene7Type = scene7TypeProperty.getValue().toString();

											if (scene7Type.equals("Image")) {
												scene7Path = scene7Domain + "is/image/" + scene7File;
											}

											else if (scene7Type.equals("Video")) {
												scene7Path = scene7Domain + "is/content/" + scene7File;
											} 
											else {
												scene7Path = scene7Domain + "is/content/" + scene7Folder+ scene7Name ;
											}
											flag=1;
										}	
									}
								}
								if(flag==1) {
									masterMap.put(element.getName(), StringEscapeUtils.unescapeHtml(scene7Path));
									log.info("inside flag if");

								}
								else
								{
									masterMap.put(element.getName(), StringEscapeUtils.unescapeHtml(element.getContent()));
								}
								Iterator<ContentVariation> iteratorvariations= element.getVariations();
								log.info(element.getName() +" : "+element.getContent());
								log.debug("iteratorvariation:"+iteratorvariations);
								if(iteratorvariations !=null)
								{
									try {
										while(iteratorvariations.hasNext()) {
											variant= iteratorvariations.next();
											if(fragmentMap.containsKey(variant.getName()))
											{
												HashMap<String,String> nextNestedMap = fragmentMap.get(variant.getName());
												nextNestedMap.put(element.getName(), StringEscapeUtils.unescapeHtml(element.getContent()));
												fragmentMap.put(variant.getName(), nextNestedMap);
											}
											else {
												HashMap<String,String> nestedMap =  new HashMap<String,String>();
												nestedMap.put(element.getName(),StringEscapeUtils.unescapeHtml(element.getContent())); 
												fragmentMap.put(variant.getName(), nestedMap);
											}
											log.info(""+fragmentMap.get(variant.getName()));
										}
									}
									catch(Exception e)
									{
										log.error("Exception iteratorvariation : ", e);
									}
								}
								flag=0;
							}
							//masterMap.put("DinoName", StringEscapeUtils.unescapeHtml(contentName));
							//masterMap.put("DinoId", StringEscapeUtils.unescapeHtml(uniqueDinoId));
							//masterMap.put("CollectionName", StringEscapeUtils.unescapeHtml(collectionName));
							//masterMap.put("Zone", StringEscapeUtils.unescapeHtml(zoneDino));
							log.info("master map");
							items.add(masterMap);
							log.debug("fragmentMap"+fragmentMap);
							if(fragmentMap!=null)
							{
								Iterator<Entry<String,HashMap<String,String>>> it = fragmentMap.entrySet().iterator();
								while(it.hasNext()) {
									Map.Entry entry = (Map.Entry)it.next();
									items.add((Map<String,String>) entry.getValue());

									it.remove();

								}

							}

						}
					}

				}
			}
		}catch(Exception e) {
			log.error("Init Exception: " ,e);
		}
		finally {
			try
			{if(resourceResolver!=null)resourceResolver.close();}
			catch(Exception e) {
				log.error("finally Exception: " ,e);

			}
		}

	}
	public List<Map<String,String>> getItems(){
		return items;
	}
}